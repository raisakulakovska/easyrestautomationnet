﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace EasyRestAutomationNet.PageObject
{
    /// <summary>
    /// This is parent class for 6 different user roles
    /// We use this class to avoid re-code, because elements have the same XPath
    /// </summary>
    public abstract class AbstractHeaderMenuComponent
    {
        protected IWebDriver Driver;

        protected AbstractHeaderMenuComponent(IWebDriver driver)
        {
            Driver = driver;
        }

        private const string ICON_BUTTON_XPATH_VALUE = "//*[@id=\"root\"]/header/div/div/div/button";

        protected IWebElement EasyRestElement => Driver.FindElement(By.CssSelector("#root > header > div > a"));

        protected IWebElement HomeButton => Driver.FindElement(By.CssSelector("[href='/']"));

        protected IWebElement RestaurantListButton => Driver.FindElement(By.CssSelector("[href='/restaurants']"));

        protected IWebElement IconButton;

        /// <summary>
        /// All user roles have logout button and its XPath is the same
        /// </summary>
        protected IWebElement LogOutButton => Driver.FindElement(By.XPath("//*[@id=\"menu-appbar\"]/div[2]/ul/li"));

        /// <summary>
        /// All user roles have one different element, that have the same XPath that others, but other text
        /// For example: AdminPanelButton, ModeratorPanelButton these two elements have the same XPath, but differents text
        /// In conclusion, we need to create 6 classes, because we have 6 roles of users
        /// </summary>
        protected string XPathForDifferentRoleButton = "//*[@id=\"menu-appbar\"]/div[2]/ul/a";

        public virtual void ClickOnEasyRest()
        {
            EasyRestElement.Click();
        }

        public virtual void ClickOnHomeButton()
        {
            HomeButton.Click();
        }

        public virtual void ClickOnRestaurantListButton()
        {
            RestaurantListButton.Click();
        }

        public virtual AbstractHeaderMenuComponent ClickOnIconButton()
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, 10));
            IconButton = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(ICON_BUTTON_XPATH_VALUE)));
            IconButton.Click();
            return this;
        }

        public virtual HomePageObject ClickOnLogOutButton()
        {
            LogOutButton.Click();
            return new HomePageObject(Driver);
        }

        public virtual string GetTextOfDifferentRoleButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            IWebElement DifferentRoleButton = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(XPathForDifferentRoleButton)));
            return DifferentRoleButton.Text;
        }

        public virtual bool IsUserLoggedIn(string buttonName)
        {
            string gottenText = GetTextOfDifferentRoleButton();
            return gottenText == buttonName;
        }
    }
}