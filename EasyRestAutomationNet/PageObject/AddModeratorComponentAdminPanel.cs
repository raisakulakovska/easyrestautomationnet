﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class AddModeratorComponentAdminPanel
    {
        private IWebDriver driver;

        public AddModeratorComponentAdminPanel(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement AddModeratorButton => driver.FindElement(By.XPath("//*[@id='root']/div/main/a"));

        private IWebElement NameField => driver.FindElement(By.XPath("//*[@id='root']/div/main/div/div/form/div/div/div[1]/div/div/div/input"));

        private IWebElement PhoneNumberField => driver.FindElement(By.XPath("//*[@id='root']/div/main/div/div/form/div/div/div[3]/div/div/div/input"));

        private IWebElement EmailField => driver.FindElement(By.XPath("//*[@id='root']/div/main/div/div/form/div/div/div[2]/div/div/div/input"));

        private IWebElement BirthDateField => driver.FindElement(By.XPath("//*[@id='root']/div/main/div/div/form/div/div/div[4]/div/div[1]/div/div/input"));

        private IWebElement PasswordField => driver.FindElement(By.XPath("//*[@id='root']/div/main/div/div/form/div/div/div[5]/div/div/div/input"));

        private IWebElement RepeatPasswordField => driver.FindElement(By.XPath("//*[@id='root']/div/main/div/div/form/div/div/div[6]/div/div/div/input"));

        private IWebElement CreateAccountButton => driver.FindElement(By.XPath("//*[@id='root']/div/main/div/div/form/div/div/div[7]/div/button"));

        private IWebElement CancelButton => driver.FindElement(By.XPath("//*[@id='root']/div/main/div/div/form/div/div/div[7]/div/a"));

        public void ClickOnAddModeratorButton()
        {
            AddModeratorButton.Click();
        }

        public void AddModerator(string name, 
                                 string phoneNumber, 
                                 string email, 
                                 string password1, 
                                 string password2, 
                                 string birthDate)
        {
            NameField.EnterInfo(name);
            PhoneNumberField.EnterInfo(phoneNumber);
            EmailField.EnterInfo(email);
            PasswordField.EnterInfo(password1);
            RepeatPasswordField.EnterInfo(password2);
            BirthDateField.EnterInfo(birthDate);
        }

        public void ClickOnCreateAccountButton()
        {
            CreateAccountButton.Click();
        }

        public void ClickOnCancelButton()
        {
            CancelButton.Click();
        }
    }
    
}

