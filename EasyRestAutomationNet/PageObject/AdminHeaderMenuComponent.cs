﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class AdminHeaderMenuComponent : AbstractHeaderMenuComponent
    {
        public AdminHeaderMenuComponent(IWebDriver driver) : base(driver)
        {

        }

        private IWebElement AdminPanelButton => Driver.FindElement(By.XPath(XPathForDifferentRoleButton));

        public void ClickOnAdminPanelButton()
        {
            AdminPanelButton.Click();
        }
    }
}