﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class AdminUsersPanelPageObject
    {
        private IWebDriver driver;

        public AdminHeaderMenuComponent AdminHeaderMenuComponent { get; set; }

        public LeftSidebarMenuAdministratorComponent LeftSidebarMenuAdministratorComponent { get; set; }

        public AdminUsersPanelPageObject(IWebDriver driver)
        {
            this.driver = driver;
            AdminHeaderMenuComponent = new AdminHeaderMenuComponent(driver);
            LeftSidebarMenuAdministratorComponent = new LeftSidebarMenuAdministratorComponent(driver);
        }
    }
}
