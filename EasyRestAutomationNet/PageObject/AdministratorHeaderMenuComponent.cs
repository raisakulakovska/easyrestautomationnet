﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class AdministratorHeaderMenuComponent : AbstractHeaderMenuComponent
    {
        public AdministratorHeaderMenuComponent(IWebDriver driver) : base(driver)
        {

        }

        private IWebElement AdministratorPanelButton => Driver.FindElement(By.XPath(XPathForDifferentRoleButton));

        public void ClickOnAdministratorPanelButton()
        {
            AdministratorPanelButton.Click();
        }
    }
}