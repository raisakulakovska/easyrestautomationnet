﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace EasyRestAutomationNet.PageObject
{
	public class AdministratorPanelPage
	{
		private readonly IWebDriver webDriver;

		private const string ORDER_CONTAINER_XPATH = "//div[contains(@class,'MuiExpansionPanelSummary-root')]";

		private IWebElement WaitingForConfirmButton => webDriver.FindElement(By.XPath("//button//span[text()='Waiting for confirm']"));

		private IWebElement AcceptedTabButton => webDriver.FindElement(By.XPath("//span[text()='Accepted']"));

		private IWebElement AssignedWaiterButton => webDriver.FindElement(By.XPath("//span[text()='Assigned waiter']"));

		private IWebElement WaitersButton => webDriver.FindElement(By.XPath("//span[text()='Waiters']"));

		public AdministratorPanelPage(IWebDriver webDriver)
		{
			this.webDriver = webDriver;
		}

		/// <summary>
		/// Clicks on Waiting for Confirm button.
		/// </summary>
		/// <returns>The list of orders that is displayed on table.</returns>
		public List<WaitingForConfirmOrderComponent> ClickOnWaitingForConfirmButton()
		{
			WaitingForConfirmButton.Click();
			List<WaitingForConfirmOrderComponent> orderList = new List<WaitingForConfirmOrderComponent>();
			var orders = webDriver.FindElements(By.XPath(ORDER_CONTAINER_XPATH));
			foreach (var order in orders)
			{
				orderList.Add(new WaitingForConfirmOrderComponent(order));
			}
			return orderList;
		}

		/// <summary>
		/// Clicks on Accepted table button.
		/// </summary>
		/// <returns>The list of orders that is displayed on table.</returns>
		public List<WaitingForAssignOrder> ClickOnAcceptedTabButton()
		{
			AcceptedTabButton.Click();
			List<WaitingForAssignOrder> orderList = new List<WaitingForAssignOrder>();
			var orders = webDriver.FindElements(By.XPath(ORDER_CONTAINER_XPATH));
			foreach (var order in orders)
			{
				orderList.Add(new WaitingForAssignOrder(order));
			}
			return orderList;
		}

		/// <summary>
		/// Clicks on Assigned table button.
		/// </summary>
		/// <returns>The list of orders that is displayed on table.</returns>
		public List<AssignedOrdersComponent> ClickOnAssignedWaiterButton()
		{
			AssignedWaiterButton.Click();
			List<AssignedOrdersComponent> orderList = new List<AssignedOrdersComponent>();
			var orders = webDriver.FindElements(By.XPath(ORDER_CONTAINER_XPATH));
			foreach (var order in orders)
			{
				orderList.Add(new AssignedOrdersComponent(order));
			}
			return orderList;
		}

		/// <summary>
		///  Clicks on Waiters table button.
		/// </summary>
		/// <returns>The list of waiters that is displayed on table.</returns>
		public List<WaiterComponent> ClickOnWaitersButton()
		{
			WaitersButton.Click();
			List<WaiterComponent> listWaiters = new List<WaiterComponent>();
			var waiters = webDriver.FindElements(By.XPath("//p[contains(text(), 'Waiter')]"));
			foreach (var waiter in waiters)
			{
				listWaiters.Add(new WaiterComponent(waiter));
			}
			return listWaiters;
		}
	}
}

