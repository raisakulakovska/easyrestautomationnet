﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
	public class AssignedOrdersComponent:OrderComponent
	{
		public AssignedOrdersComponent(IWebElement webElement) : base(webElement)
		{

		}

		/// <summary>
		/// Click on Expand button on Assigned Waiter table.
		/// </summary>
		public void ExpandOrder()
		{
			ExpandCollapseButton.Click();
		}
	}
}
