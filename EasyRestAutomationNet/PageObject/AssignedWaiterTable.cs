﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace EasyRestAutomationNet.PageObject
{
    public class AssignedWaiterTable : WaiterPanel
    {
        public AssignedWaiterTable(IWebDriver driver) : base(driver) { }

        public List<OrderItemComponent> GetCurrentOrders()
        {
            List<OrderItemComponent> currentOrders = new List<OrderItemComponent>();

            ClickOnInProgressTable();

            currentOrders.AddRange(GetOrders());

            ClickOnAssignedWaiterTableButton();

            currentOrders.AddRange(GetOrders());

            return currentOrders;
        }
    }
}
