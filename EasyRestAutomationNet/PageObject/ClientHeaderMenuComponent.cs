﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace EasyRestAutomationNet.PageObject
{
    public class ClientHeaderMenuComponent : AbstractHeaderMenuComponent
    {
        public ClientHeaderMenuComponent(IWebDriver driver) : base(driver)
        {

        }

        private IWebElement MyProfileButton => Driver.FindElement(By.XPath(XPathForDifferentRoleButton));

        private IWebElement LogOutButton;

        public void ClickOnMyProfileButton()
        {
            MyProfileButton.Click();
        }

        public void ClickOnLogOutButton()
        {
            var wait = new WebDriverWait(Driver, new TimeSpan(0, 0, 10));
            LogOutButton = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='menu-appbar']/div[2]/ul/li")));
            LogOutButton.Click();
        }
    }
}