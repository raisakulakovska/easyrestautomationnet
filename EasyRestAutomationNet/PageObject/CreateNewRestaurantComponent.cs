﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace EasyRestAutomationNet.PageObject
{
    public class CreateNewRestaurantComponent
    {
        private IWebElement element;

        public CreateNewRestaurantComponent(IWebElement element)
        {
            this.element = element;
        }

        private IWebElement NameInputField => element.FindElement(By.XPath("/div[2]/form/div/div[1]/div/div/div/input"));

        private IWebElement AddressInputField => element.FindElement(By.XPath("/div[2]/form/div/div[2]/div/div/div/input"));

        private IWebElement PhoneInputField => element.FindElement(By.XPath("/div[2]/form/div/div[3]/div/div/input"));

        private IWebElement PreviewTextInputField => element.FindElement(By.XPath("/div[2]/form/div/div[4]/div/div/textarea"));

        private IWebElement DescriptionInputField => element.FindElement(By.XPath("/div[2]/form/div/div[7]/div/div[2]/div/div/div/div/div/div/div/span"));

        private IWebElement AddButton => element.FindElement(By.XPath("/div[2]/form/div/div[9]/button"));

        private IWebElement CancelButton => element.FindElement(By.XPath("/div[2]/form/div/div[8]/button"));

        private IWebElement ComboBoxTags => element.FindElement(By.Id("select-tags"));

        private void AddTagForRestaurant(string tagNameForClick)
        {
            // Find element by tag name and then do /../../ and click on checkbox.
            element.FindElement(By.XPath($"//*[@id=\"menu-tags\"]/div[2]/ul//li/div/span[text()='{tagNameForClick}']/../../span[1]")).Click();
        }

        public void AddRestaurant(string name,
                                  string address,
                                  string phone,
                                  string previewText,
                                  string description,
                                  List<string> tags)
        {
            NameInputField.EnterInfo(name);

            AddressInputField.EnterInfo(address);

            PhoneInputField.EnterInfo(phone);

            PreviewTextInputField.EnterInfo(previewText);

            DescriptionInputField.EnterInfo(description);

            // Open list with tags.
            ComboBoxTags.Click();

            for (int i = 0; i < tags.Count; ++i)
            {
                AddTagForRestaurant(tags[i]);
            }

            // For saving checkbox changes we need to click somewhere in the screen.
            NameInputField.Click();

            AddButton.Click();
        }
    }
}