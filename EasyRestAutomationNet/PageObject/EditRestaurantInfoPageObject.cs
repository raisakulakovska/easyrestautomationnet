﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace EasyRestAutomationNet.PageObject
{
    public class EditRestaurantInfoPageObject
    {
        private IWebDriver driver;

        public EditRestaurantInfoPageObject(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement EditInformationButton => driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/button"));

        private IWebElement EditNameInputField => driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[3]/div/div/div/div[2]/form/div/div[1]/div/div/div/input"));

        private IWebElement EditAddressInputField => driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[3]/div/div/div/div[2]/form/div/div[2]/div/div/div/input"));

        private IWebElement EditPhoneInputField => driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[3]/div/div/div/div[2]/form/div/div[3]/div/div/input"));

        private IWebElement EditPreviewTextInputField => driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[3]/div/div/div/div[2]/form/div/div[4]/div/div/textarea"));

        private IWebElement EditDescriptionInputField => driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[3]/div/div/div/div[2]/form/div/div[6]/div/div[2]/div/div/div/div/div/div/div/span/span"));

        private IWebElement UdpateButton => driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[3]/div/div/div/div[2]/form/div/div[9]/button"));

        private IWebElement ComboBoxTags => driver.FindElement(By.Id("select-tags"));

        // List of pressed combo boxes displays in ComboBoxTags element as a string.
        private string[] SeparatedTagsForClear { get; set; }

        private void ClearAllComboBoxTags()
        {
            for (int i = 0; i < SeparatedTagsForClear.Length; ++i)
            {
                // Searching pressed combobox by text and unchecked it.
                driver.FindElement(By.XPath($"//*[@id=\"menu-tags\"]/div[2]/ul//li/div/span[text()='{SeparatedTagsForClear[i]}']/../../span[1]")).Click();
            }
        }

        /// <summary>
        /// Make combobox with tag checked.
        /// </summary>
        private void AddTagForRestaurant(string tagNameForClick)
        {
            driver.FindElement(By.XPath($"//*[@id=\"menu-tags\"]/div[2]/ul//li/div/span[text()='{tagNameForClick}']/../../span[1]")).Click();
        }

        public void EditRestaurant(string name,
                                   string address,
                                   string phone,
                                   string previewText,
                                   string description,
                                   List<string> tags)
        {
            // Open form for editing.
            EditInformationButton.Click();

            EditNameInputField.EnterInfo(name);

            EditAddressInputField.EnterInfo(address);

            EditPhoneInputField.EnterInfo(phone);

            EditPreviewTextInputField.EnterInfo(previewText);

            EditDescriptionInputField.EnterInfo(description);

            // Reading already checked tags.
            SeparatedTagsForClear = ComboBoxTags.Text.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // Open list with tags.
            ComboBoxTags.Click();

            // Clear previous tags.
            ClearAllComboBoxTags();

            // Make checkboxes-checked with tag name.
            for (int i = 0; i < tags.Count; ++i)
            {
                AddTagForRestaurant(tags[i]);
            }

            // For saving checkbox changes we need to click somewhere in the screen.
            EditNameInputField.Click();

            UdpateButton.Click();
        }
    }
}
