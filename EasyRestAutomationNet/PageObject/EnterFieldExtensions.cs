﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public static class EnterFieldExtensions
    {
        public static IWebElement EnterInfo(this IWebElement inputField, string text)
        {
            inputField.Clear();
            inputField.Click();
            inputField.SendKeys(text);
            return inputField;
        }
    }
}
