﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class HeaderMenuPageComponent
    {
        private IWebDriver driver;

        public HeaderMenuPageComponent(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement EasyRestElement => driver.FindElement(By.CssSelector("#root > header > div > a"));

        private IWebElement HomeButton => driver.FindElement(By.CssSelector("[href='/']"));

        private IWebElement RestaurantListButton => driver.FindElement(By.CssSelector("[href='/restaurants']"));

        private IWebElement SignInButton => driver.FindElement(By.CssSelector("[href='/log-in']"));

        private IWebElement SignUpButton => driver.FindElement(By.CssSelector("[href='/sign-up']"));

        public void ClickOnEasyRest()
        {
            EasyRestElement.Click();
        }

        public void ClickOnHomeButton()
        {
            HomeButton.Click();
        }

        public void ClickOnRestaurantListButton()
        {
            RestaurantListButton.Click();
        }

        public SignInPageObject ClickOnSignInButton()
        {
            SignInButton.Click();
            return new SignInPageObject(driver);
        }

        public SignUpPageObject ClickOnSignUpButton()
        {
            SignUpButton.Click();
            return new SignUpPageObject(driver);
        }

        public bool IsSignInButtonDisplayed()
        {
            return SignInButton.Displayed;
        }

        public bool IsSignUpButtonDisplayed()
        {
            return SignUpButton.Displayed;
        }

        public bool IsHomeButtonDisplayed()
        {
            return HomeButton.Displayed;
        }

        public bool IsRestaurantListButtonDisplayed()
        {
            return RestaurantListButton.Displayed;
        }
    }
}