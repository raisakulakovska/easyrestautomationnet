﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    /// <summary>
    /// For a now we don't have requirements for using restaurant categories
    /// So i create only HomePageObject with requirements for Unauthorized user(HeaderMenuPageComponent)
    /// This is start component with sign in and sign up buttons.
    /// </summary>
    public class HomePageObject
    {
        private IWebDriver driver;

        public HeaderMenuPageComponent HeaderMenuPageComponent { get; set; }

        public HomePageObject(IWebDriver driver)
        {
            this.driver = driver;
            HeaderMenuPageComponent = new HeaderMenuPageComponent(driver);
        }
    }
}