﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class InProgressTable : WaiterPanel
    {
        public InProgressTable(IWebDriver driver) : base(driver) { }

        private IWebElement CloseOrderButton(int orderNumber)
            => Driver.FindElement(By.XPath($"//*[@id=\"root\"]/main/div/div[{orderNumber}]/div/div[2]/div/div/div[2]/button"));

        public void ClickOnCloseOrderButton(int orderNumber)
        {
            CloseOrderButton(GetOrderID(orderNumber)).Click();
        }
    }
}
