﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class LeftSidebarMenuAdministratorComponent
    {
        private IWebDriver driver;
        public LeftSidebarMenuAdministratorComponent(IWebDriver driver)
        {
            this.driver = driver;
        }

        // initialising UI elements Left sidebar menu Administrator:
        private IWebElement UsersButton => driver.FindElement(By.CssSelector("[href = '/admin/users']"));

        private IWebElement OwnersButton => driver.FindElement(By.CssSelector("[href = '/admin/owners']"));

        private IWebElement ModeratorsButton => driver.FindElement(By.CssSelector("[href = '/admin/moderators']"));

        private IWebElement RestaurantsButton => driver.FindElement(By.CssSelector("[href = '/admin/restaurants']"));

        // methods for cliking on Left sidebar menu Admin buttons:
        public TableWithUsersOnAdminPanelComponent ClickOnUsersButton()
        {
            UsersButton.Click();
            var container = driver.FindElement(By.TagName("main"));
            return new TableWithUsersOnAdminPanelComponent(container);
        } 

        public TableWithUsersOnAdminPanelComponent ClickOnOwnersButton()
        {
            OwnersButton.Click();
            var container = driver.FindElement(By.TagName("main"));
            return new TableWithUsersOnAdminPanelComponent(container);
        } 
        
        public TableWithUsersOnAdminPanelComponent ClickOnModeratorsButton()
        {
            ModeratorsButton.Click();
            var container = driver.FindElement(By.TagName("main"));
            return new TableWithUsersOnAdminPanelComponent(container);
        }

        public RestaurantsComponentsAdminPanel ClickOnRestaurantsButton()
        {
            RestaurantsButton.Click();
            var container = driver.FindElement(By.TagName("main"));
            return new RestaurantsComponentsAdminPanel(container);
        } 
    }
}