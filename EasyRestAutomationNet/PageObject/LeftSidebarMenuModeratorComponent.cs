﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class LeftSidebarMenuModeratorComponent
    {
        private IWebElement leftSideBarMenuModeratorContainer; 

        public LeftSidebarMenuModeratorComponent(IWebElement leftSideBarMenuModeratorContainer)
        {
            this.leftSideBarMenuModeratorContainer = leftSideBarMenuModeratorContainer;
        }

        // initialising UI elements Left sidebar menu Moderator
        private IWebElement RestaurantsButton => leftSideBarMenuModeratorContainer.FindElement(By.CssSelector("[href='/moderator/restaurants']"));

        private IWebElement UsersButton => leftSideBarMenuModeratorContainer.FindElement(By.CssSelector("[href='/moderator/users']"));

        private IWebElement OwnersButton => leftSideBarMenuModeratorContainer.FindElement(By.CssSelector("[href='/moderator/owners']"));

        // Methods for cliking on Left sidebar menu Moderator
        public void ClickOnRestaurantsButton()
        {
            RestaurantsButton.Click();
        }

        public void ClickOnUsersButton()
        {
            UsersButton.Click();
        }

        public void ClickOnOwnersButton()
        {
            OwnersButton.Click();
        }
    }
}
