﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class ModeratorHeaderMenuComponent : AbstractHeaderMenuComponent
    {
        public ModeratorHeaderMenuComponent(IWebDriver driver) : base(driver)
        {

        }

        private IWebElement ModeratorPanelButton => Driver.FindElement(By.XPath(XPathForDifferentRoleButton));

        public void ClickOnModeratorPanelButton()
        {
            ModeratorPanelButton.Click();
        }
    }
}