﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    // Just initializing UI elements on Profile page
    public class MyProfilePageObject
    {
        private IWebDriver webDriver;

        public MyProfilePageObject(IWebDriver driver)
        {
            webDriver = driver;
            ClientHeaderMenuComponent = new ClientHeaderMenuComponent(driver);
        }

        public ClientHeaderMenuComponent ClientHeaderMenuComponent { get; set; }

        private const string NAME_XPATH_VALUE = "//[@id='root']/main/div/div/div/div[1]/div/div[2]/div/table/tbody/tr[1]/td";

        private const string BIRTH_DATE_XPATH_VALUE = "//*[@id='root']/main/div/div/div/div[1]/div/div[2]/div/table/tbody/tr[2]/td";

        private const string EMAIL_XPATH_VALUE = "//*[@id='root']/main/div/div/div/div[1]/div/div[2]/div/table/tbody/tr[3]/td";

        private const string PHONE_NUMBER_XPATH_VALUE = "//*[@id='root']/main/div/div/div/div[1]/div/div[2]/div/table/tbody/tr[4]/td";

        private const string NAME_UNDER_PHOTO_XPATH_VALUE = "//*[@id='root']/main/div/div/div/div[1]/div/div[1]/div[3]/h2";

        private const string ROLE_UNDER_PHOTO_XPATH_VALUE = "//*[@id='root']/main/div/div/div/div[1]/div/div[1]/div[3]/p";

        protected IWebElement Name => webDriver.FindElement(By.XPath(NAME_XPATH_VALUE));

        protected IWebElement BirthDate => webDriver.FindElement(By.XPath(BIRTH_DATE_XPATH_VALUE));

        protected IWebElement Email => webDriver.FindElement(By.XPath(EMAIL_XPATH_VALUE));

        protected IWebElement PhoneNumber => webDriver.FindElement(By.XPath(PHONE_NUMBER_XPATH_VALUE));

        protected IWebElement NameUnderPhoto => webDriver.FindElement(By.XPath(NAME_UNDER_PHOTO_XPATH_VALUE));

        protected IWebElement RoleUnderPhoto => webDriver.FindElement(By.XPath(ROLE_UNDER_PHOTO_XPATH_VALUE));
    }
}