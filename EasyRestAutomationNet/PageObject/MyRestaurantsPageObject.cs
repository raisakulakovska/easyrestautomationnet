﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class MyRestaurantsPageObject
    {
        private IWebDriver driver;

        public MyRestaurantsPageObject(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement AddRestaurantButton => driver.FindElement(By.XPath("//*[@id=\"root\"]/main/div/div/div/div[1]/div[2]/button"));

        private IWebElement GetRestaurantByName(string restaurantName)
        {
            // We are searching restaurant by name in h2 and return full element.
            string xPathToFoundRestaurantByName = $"//main/div/div/div/div[1]/div[1]/div/div/div[2]/div/div[1]/h2[text()='{restaurantName}']/../../../../..";

            return driver.FindElement(By.XPath(xPathToFoundRestaurantByName));
        }

        public void RemoveRestaurantByName(string restaurantName)
        {
            IWebElement restaurantForRemove = GetRestaurantByName(restaurantName);

            // We are searching moreActionsWithRestaurantButton from certain restaurant.
            // So it will return only one element, not all elements on the page.
            IWebElement moreActionsWithRestaurantButton = restaurantForRemove.FindElement(By.XPath("//button[contains(@class,'MuiButtonBase-root')]"));

            moreActionsWithRestaurantButton.Click();

            // This element shows only after a click on moreActionsWithRestaurantButton.
            // And we can click only in one moreActionsWithRestaurantButton at the page.
            // So we will click on certain archive button.
            IWebElement archiveButton = driver.FindElement(By.XPath("//span[text()='Archive']/../.."));

            archiveButton.Click();
        }

        public CreateNewRestaurantComponent ClickOnAddRestaurantButton()
        {
            AddRestaurantButton.Click();

            IWebElement newRestaurantContainer = driver.FindElement(By.XPath("//*[@id=\"root\"]/main/div/div/div/div[1]/div[3]/div/div/div"));

            return new CreateNewRestaurantComponent(newRestaurantContainer);
        }

        public EditRestaurantInfoPageObject FindRestaurantByNameAndClickOnManageButton(string restaurantName)
        {
            IWebElement foundRestaurant = GetRestaurantByName(restaurantName);

            // Click on the three dots(more actions) button from a founded restaurant.
            IWebElement moreActionsWithRestaurantButton = foundRestaurant.FindElement(By.XPath("//button[contains(@class,'MuiButtonBase-root')]"));

            moreActionsWithRestaurantButton.Click();

            // This element shows only after a click on moreActionsWithRestaurantButton.
            // And we can click only in one moreActionsWithRestaurantButton at the page.
            // So we'll click on certain manage button.
            // After pressing this button, the driver will open a new page.
            IWebElement manageButton = driver.FindElement(By.XPath("//span[text()='Manage']/../.."));

            manageButton.Click();

            return new EditRestaurantInfoPageObject(driver);
        }
    }
}