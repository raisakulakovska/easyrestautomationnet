﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
	 public abstract class OrderComponent
	 {
		protected readonly IWebElement webElement;

		protected IWebElement OrderId => webElement.FindElement(By.XPath("//p[text()='Order id: #']"));

		protected IWebElement User => webElement.FindElement(By.XPath("//p[text()='User: ']"));

		protected IWebElement ExpandCollapseButton => webElement.FindElement(By.XPath("//span[contains(@class,'MuiIconButton')]"));

		protected OrderComponent(IWebElement webElement)
		{
			this.webElement = webElement;
		}
	 }
}

