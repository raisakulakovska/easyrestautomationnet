﻿namespace EasyRestAutomationNet.PageObject
{
    public class OrderItemComponent
    {
        public readonly string ClientName;

        public readonly string MobilePhone;

        public readonly string Email;

        public readonly int OrderNum;

        public readonly double OrderPrice;

        public OrderItemComponent(string name, string phone, string email, int number, double price)
        {
            ClientName = name;
            MobilePhone = phone;
            Email = email;
            OrderNum = number;
            OrderPrice = price;
        }

        public override string ToString()
        {
            return "Name: " + ClientName
                  + "\nPhone: " + MobilePhone
                  + "\nEmail: " + Email
                  + "\nNumber: " + OrderNum
                  + "\nPrice: " + OrderPrice;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                OrderItemComponent p = (OrderItemComponent)obj;
                return p.MobilePhone == this.MobilePhone &&
                       p.OrderNum == this.OrderNum &&
                       p.OrderPrice == this.OrderPrice &&
                       p.Email == this.Email &&
                       p.ClientName == this.ClientName;
            }
        }
    }
}
