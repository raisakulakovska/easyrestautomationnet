﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace EasyRestAutomationNet.PageObject
{
	public class OrderSummaryAssignWaiterComponent
	{
		private readonly IWebElement webElement;

		private IWebElement AssignButton => webElement.FindElement(By.XPath("//span[text()='Assign']"));

		private IEnumerable<IWebElement> RadioButtons => webElement.FindElements(By.XPath("//input"));

		public OrderSummaryAssignWaiterComponent(IWebElement webElement)
		{
			this.webElement = webElement;
		}

		/// <summary>
		/// Clicks on Assign button at Order summary table. 
		/// </summary>
		/// <returns>Order Summary table.</returns>
		public OrderSummaryAssignWaiterComponent ClickOnAssignButton()
		{
			AssignButton.Click();
			return this;
		}

		/// <summary>
		/// Picks waiter by clicking on radio button.
		/// </summary>
		/// <param name="waiterId"> Id of waiter that should be picked</param>
		/// <returns>Order Summary table.</returns>
		public OrderSummaryAssignWaiterComponent PickWaiter(int waiterId)
		{
			RadioButtons.ElementAt(waiterId-1).Click();
			return this;
		}
	}
}

