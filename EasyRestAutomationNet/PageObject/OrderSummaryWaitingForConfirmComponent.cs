﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
	public class OrderSummaryWaitingForConfirmComponent
	{
		IWebElement webElement;

		private IWebElement AcceptButton => webElement.FindElement(By.XPath("//span[text()='Accept']"));

		public OrderSummaryWaitingForConfirmComponent(IWebElement webElement)
		{
			this.webElement = webElement;
		}

		/// <summary>
		/// Click on Accept button at Order Summary table. 
		/// </summary>
		/// <returns>Order Summary table.</returns>
		public OrderSummaryWaitingForConfirmComponent ClickOnAcceptButton()
		{
			AcceptButton.Click();
			return this;
		}
	}
}
