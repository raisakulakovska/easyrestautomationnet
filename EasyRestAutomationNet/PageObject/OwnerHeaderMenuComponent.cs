﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class OwnerHeaderMenuComponent : AbstractHeaderMenuComponent
    {
        public OwnerHeaderMenuComponent(IWebDriver driver) : base(driver)
        {

        }

        private IWebElement MyRestaurantsButton => Driver.FindElement(By.XPath(XPathForDifferentRoleButton));

        public void ClickOnMyRestaurantsButton()
        {
            MyRestaurantsButton.Click();
        }
    }
}