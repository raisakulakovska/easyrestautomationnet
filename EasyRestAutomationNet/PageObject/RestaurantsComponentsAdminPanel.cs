﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class RestaurantsComponentsAdminPanel
    {
        private IWebElement parent;

        public RestaurantsComponentsAdminPanel(IWebElement parent)
        {
            this.parent = parent;
        }
        
        private IWebElement AllRestaurantsButton => parent.FindElement(By.XPath("div[1]/header/div/div[2]/div[2]/div/button[1]"));

        private IWebElement UnapprovedRestaurantsButton => parent.FindElement(By.XPath("div[1]/header/div/div[2]/div[2]/div/button[2]"));

        private IWebElement ApprovedRestaurantsButton => parent.FindElement(By.XPath("div[1]/header/div/div[2]/div[2]/div/button[3]"));

        private IWebElement ArchivedRestaurantsButton => parent.FindElement(By.XPath("div[1]/header/div/div[2]/div[2]/div/button[4]"));
        
        public void ClickOnAllRestaurantsButton()
        {
            AllRestaurantsButton.Click();
        }
        
        public void ClickOnUnapprovedRestaurantsButton()
        {
            UnapprovedRestaurantsButton.Click();
        }
        
        public void ClickOnApprovedRestaurantsButton()
        {
            ApprovedRestaurantsButton.Click();
        }
        
        public void ClickOnArchivedRestaurantsButton()
        {
            ArchivedRestaurantsButton.Click();
        }   

    }
}