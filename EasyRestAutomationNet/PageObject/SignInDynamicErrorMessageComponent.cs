﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class SignInDynamicErrorMessageComponent
    {
        private IWebDriver browser;

        public SignInDynamicErrorMessageComponent(IWebDriver browser)
        {
            this.browser = browser;
        }

        private IWebElement DynamicErrorMessage => browser.FindElement(By.Id("client-snackbar"));

        public bool IsDynamicErrorMessageDisplayed()
        {
            return DynamicErrorMessage.Displayed;
        }
    }
}