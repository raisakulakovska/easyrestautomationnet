﻿using EasyRestAutomationNet.Tests;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace EasyRestAutomationNet.PageObject
{
    public class SignInPageObject
    {
        private IWebDriver webDriver;

        public SignInDynamicErrorMessageComponent SignInDynamicErrorMessageComponent { get; set; }

        public SignInPageObject(IWebDriver driver)
        {
            webDriver = driver;
            SignInDynamicErrorMessageComponent = new SignInDynamicErrorMessageComponent(webDriver);
        }

        // Placing XPathes into constants
        private const string EMAIL_XPATH_VALUE = "//input[@name='email']";

        private const string PASSWORD_XPATH_VALUE = "//input[@name='password']";

        private const string SIGN_IN_BUTTON_XPATH_VALUE = "//span[text()='Sign In']";

        private const string ERROR_MESSAGE_XPATH_VALUE = "//*[@id=\"root\"]/main/div/div[2]/form/div/div[1]/div/p";

        private const string DYNAMIC_ERROR_MESSAGE_XPATH_VALUE = "//*[@id=\"client-snackbar\"]";

        // UI elements initialising
        private IWebElement EmailInput => webDriver.FindElement(By.XPath(EMAIL_XPATH_VALUE));

        private IWebElement PasswordInput => webDriver.FindElement(By.XPath(PASSWORD_XPATH_VALUE));

        private IWebElement SignInButton => webDriver.FindElement(By.XPath(SIGN_IN_BUTTON_XPATH_VALUE));

        // Method for user's logining
        public MyProfilePageObject Login(string emailInput, string passwordInput)
        {
            EmailInput.EnterInfo(emailInput);

            PasswordInput.EnterInfo(passwordInput);

            SignInButton.Click();
            return new MyProfilePageObject(webDriver);
        }

        public virtual string GetTextOfErrorMessage()
        {
            try
            {
                WebDriverWait waiter = new WebDriverWait(webDriver, TimeSpan.FromSeconds(5));
                IWebElement errorMessage = waiter.Until(ExpectedConditions.ElementIsVisible(By.XPath(ERROR_MESSAGE_XPATH_VALUE)));
                return errorMessage.Text;
            }
            catch(OpenQA.Selenium.WebDriverTimeoutException)
            {
                return "";
            }
        }

        public virtual bool IfErrorExist(string errorMessage)
        {
            string errorText = GetTextOfErrorMessage();
            return errorText == errorMessage;
        }

        public virtual string GetTextOfDynamicErrorMessage()
        {
            try
            {
                WebDriverWait waiter = new WebDriverWait(webDriver, TimeSpan.FromSeconds(5));
                IWebElement dinamicErrorMessage = waiter.Until(ExpectedConditions.ElementIsVisible(By.XPath(DYNAMIC_ERROR_MESSAGE_XPATH_VALUE)));
                return dinamicErrorMessage.Text;
            }
            catch (OpenQA.Selenium.WebDriverTimeoutException)
            {
                return "";
            }
        }

        public virtual bool IfDynamicErrorExist(string errorMessage)
        {
            string errorText = GetTextOfDynamicErrorMessage();
            return errorText == errorMessage;
        }
    }
}