using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{   
    public class SignUpPageObject
    {
        private IWebDriver webDriver;

        public SignUpPageObject(IWebDriver driver)
        {
            webDriver = driver;
        }

        // XPathes into constants
        private const string NAME_XPATH_VALUE = "//input[@name='name']";

        private const string PHONE_XPATH_VALUE = "//input[@name='phoneNumber']";

        private const string PASSWORD_XPATH_VALUE = "//input[@name='password']";

        private const string CONFIRM_PASSVORD_XPATH_VALUE = "//input[@name='repeated_password']";

        private const string EMAIL_XPATH_VALUE = "//input[@name='email']";

        private const string BIRTH_DATE_XPATH_VALUE = "//input[@name='birthDate']";

        private const string SIGN_UP_BUTTON_XPATH_VALUE = "//span[text()='Create account']";

        // Initialise UI elements
        private IWebElement NameInput => webDriver.FindElement(By.XPath(NAME_XPATH_VALUE));

        private IWebElement PhoneNumberInput => webDriver.FindElement(By.XPath(PHONE_XPATH_VALUE));

        private IWebElement PasswordInput => webDriver.FindElement(By.XPath(PASSWORD_XPATH_VALUE));

        private IWebElement ConfirmPasswordInput => webDriver.FindElement(By.XPath(CONFIRM_PASSVORD_XPATH_VALUE));

        private IWebElement EmailInput => webDriver.FindElement(By.XPath(EMAIL_XPATH_VALUE));

        private IWebElement BirthDateInput => webDriver.FindElement(By.XPath(BIRTH_DATE_XPATH_VALUE));

        private IWebElement SignUpButton => webDriver.FindElement(By.XPath(SIGN_UP_BUTTON_XPATH_VALUE));

        private string birthDate = "1983-10-26";

        // Method for setting data value
        private void SetDataValue(IWebElement BirthDateInput, string birthDate)
        {            
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript($"{BirthDateInput}.setAttribute('value', {birthDate})");
        }

        // Method for registering a new user
        public HomePageObject SignUp(string nameInput,
                                     string phoneNumberInput,
                                     string passwordInput,
                                     string confirmPasswordInput,
                                     string emailInput)
        {
            NameInput.EnterInfo(nameInput);

            PhoneNumberInput.EnterInfo(phoneNumberInput);

            PasswordInput.EnterInfo(passwordInput);

            ConfirmPasswordInput.EnterInfo(confirmPasswordInput);

            EmailInput.EnterInfo(emailInput);

            SetDataValue(BirthDateInput, birthDate);

            SignUpButton.Click();
            return new HomePageObject(webDriver);
        }
    }
}