﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace EasyRestAutomationNet.PageObject
{
    public class TableWithUsersOnAdminPanelComponent
    {
        private IWebElement parent;

        public TableWithUsersOnAdminPanelComponent(IWebElement parent)
        {
            this.parent = parent;
        }

        private IWebElement AllButton => parent.FindElement(By.XPath("div[1]/header/div/div[2]/div[2]/div/button[1]"));

        private IWebElement ActiveButton => parent.FindElement(By.XPath("div[1]/header/div/div[2]/div[2]/div/button[2]"));

        private IWebElement BannedButton => parent.FindElement(By.XPath("div[1]/header/div/div[2]/div[2]/div/button[3]"));

        public TableWithUsersOnAdminPanelComponent ClickOnAllButton()
        {
            AllButton.Click();
            return this;
        }

        public TableWithUsersOnAdminPanelComponent ClickOnActiveButton()
        {
            ActiveButton.Click();
            return this;
        }

        public TableWithUsersOnAdminPanelComponent ClickOnBannedButton()
        {
            BannedButton.Click();
            return this;
        }

        public UserInfoComponent FindUserByEmail(string email)
        {
            UserInfoComponent userComponent = null;
            var users = GetUsers();
            foreach (UserInfoComponent user in users)
            {
                if (user.GetEmail() == email)
                {
                    userComponent = user;
                    break;
                }
            }
            return userComponent;
        }

        public List<UserInfoComponent> GetUsers()
        {
            var wait = new DefaultWait<IWebElement>(parent);
            wait.Timeout = TimeSpan.FromSeconds(3);
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            IWebElement table  = wait.Until(x => x.FindElement(By.XPath("div[2]/table")));
            IWebElement tableBody = table.FindElement(By.TagName("tbody"));
            IList<IWebElement> tableRows = tableBody.FindElements(By.TagName("tr"));
            var rowComponents = new List<UserInfoComponent>();
            foreach (IWebElement row in tableRows)
            {
                rowComponents.Add(new UserInfoComponent(row));
            }
            return rowComponents;
        }
    }
}

