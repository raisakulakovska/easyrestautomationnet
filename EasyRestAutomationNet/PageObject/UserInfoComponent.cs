﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class UserInfoComponent
    {
        private IWebElement rowParentContainer;

        public UserInfoComponent(IWebElement parent) 
        {
            this.rowParentContainer = parent;
        }

        private IWebElement EmailField => rowParentContainer.FindElement(By.XPath("td[1]"));

        private IWebElement ActionButton => rowParentContainer.FindElement(By.TagName("button"));

        public string GetEmail()
        {
            return EmailField.Text;
        }

        public void ClickOnButton()
        {
            ActionButton.Click();
        }

        public bool IsActive()
        {
            return ActionButton.GetAttribute("class").Contains("colorPrimary");
        }

        public bool IsBanned()
        {
            return ActionButton.GetAttribute("class").Contains("colorSecondary");
        }
    }
}
