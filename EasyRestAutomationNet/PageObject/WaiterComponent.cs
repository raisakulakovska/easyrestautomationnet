﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace EasyRestAutomationNet.PageObject
{
	public class WaiterComponent
	{
		private readonly IWebElement webElement;

		private IWebElement WaiterName => webElement;

		private IWebElement ExpandButton => webElement.FindElement(By.XPath("//parent::div/parent::div/parent::div/following-sibling::div//span[contains(@class, 'MuiIconButton')]"));

		public WaiterComponent(IWebElement webElement)
		{
			this.webElement = webElement;
		}

		/// <summary>
		/// Click on Expand button.
		/// </summary>
		/// <returns>The list of orders that is displayed on table.</returns>
		public List<AssignedOrdersComponent> ClickOnWaitersExpandButton()
		{
			ExpandButton.Click();
			return ReadTable();
		}

		/// <summary>
		/// Fills list with table data.
		/// </summary>
		/// <returns>The list of orders that is displayed on table.</returns>
		private List<AssignedOrdersComponent> ReadTable()
		{
			List<AssignedOrdersComponent> orderList = new List<AssignedOrdersComponent>();
			var orders = webElement.FindElements(By.XPath("//parent::div/parent::div/parent::div/parent::div/following-sibling::div/div/div/div/div/div"));
			foreach (var order in orders)
			{
				orderList.Add(new AssignedOrdersComponent(order));
			}
			return orderList;
		}
	}
}
