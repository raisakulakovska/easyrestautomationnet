﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
    public class WaiterHeaderMenuComponent : AbstractHeaderMenuComponent
    {
        public WaiterHeaderMenuComponent(IWebDriver driver) : base(driver)
        {

        }

        private IWebElement WaiterPanelButton => Driver.FindElement(By.XPath(XPathForDifferentRoleButton));

        public void ClickOnWaiterPanelButton()
        {
            WaiterPanelButton.Click();
        }
    }
}