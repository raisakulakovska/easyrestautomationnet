﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EasyRestAutomationNet.PageObject
{
    public abstract class WaiterPanel
    {
        protected readonly IWebDriver Driver;

        protected const string TABLE_ITEM_XPATH = "//*[@id=\"root\"]/main/div/div";

        protected WaiterPanel(IWebDriver webDriver)
        {
            Driver = webDriver;
        }

        private IJavaScriptExecutor javaScriptExecutor => (IJavaScriptExecutor) Driver;

        private IWebElement AllTableButton =>
            Driver.FindElement(By.XPath("//*[@id=\"root\"]/main/header/div/div[2]/div[2]/div/a[1]/span[1]/span/span"));
        
        private IWebElement AssignedWaiterTableButton =>
            Driver.FindElement(By.XPath("//*[@id=\"root\"]/main/header/div/div[2]/div[2]/div/a[2]/span[1]/span/span"));
        
        private IWebElement InProgressTable => 
            Driver.FindElement(By.XPath("//*[@id=\"root\"]/main/header/div/div[2]/div[2]/div/a[3]/span[1]/span/span"));
       
        private IWebElement HistoryTableButton =>
            Driver.FindElement(By.XPath("//*[@id=\"root\"]/main/header/div/div[2]/div[2]/div/a[4]/span[1]/span/span"));
       
        private IList<IWebElement> OrdersWebElements => Driver.FindElements(By.XPath(TABLE_ITEM_XPATH));
        
        private IWebElement ExpandOrderButton(int orderNumber) =>
            Driver.FindElement(By.XPath($"//*[@id=\"root\"]/main/div/div[{orderNumber}]/div/div/div[2]/div/div[4]/div/button"));
       
        private IWebElement CollapseOrderButton(int orderNumber) =>
            Driver.FindElement(By.XPath($"//*[@id=\"root\"]/main/div/div[{orderNumber}]/div/div[1]/div[2]/div/div[4]/div/button"));
        
        private List<OrderItemComponent> Orders => GetOrders();

        public void ScrolltoTopOfThePage()=>
            javaScriptExecutor.ExecuteScript("window.scrollTo(0, 0)");

        public void ClickOnExpandOrderButton(int id) =>
            ExpandOrderButton(GetOrderID(id)).Click();
     
        public void ClickOnCollapseOrderButton(int id) =>
            CollapseOrderButton(GetOrderID(id)).Click();
       
        public void ClickOnAssignedWaiterTableButton()
        {
            AssignedWaiterTableButton.Click();
        }

        public void ClickOnAllTableButton()
        {
            AllTableButton.Click();
        }

        public void ClickOnInProgressTable()
        {
            InProgressTable.Click();
        }

        public void ClickOnHistoryTableButton()
        {
            HistoryTableButton.Click();
        }
        
        // Every order has it's number (For example №55, you can see it on the page). 
        // This function returns the index of the needed order from the list of Order Items         
        protected int GetOrderID(int orderNumber)
        {
            OrderItemComponent item = GetOrders().FirstOrDefault(x => x.OrderNum == orderNumber);
            return Orders.IndexOf(item) + 1;
        }
       
        // Converts the Order Item from IWebElement type to OrderItem type
        private OrderItemComponent ToOrderItem(IWebElement orderItem)
        {
            string itemText = orderItem.Text;

            int indexOfName = itemText.IndexOf("Client: ") + 8;

            int indexOfPhone = itemText.IndexOf("phone: ") + 7;

            int indexOfEmail = itemText.IndexOf("email: ") + 7;

            int indexOfOrderNumber = itemText.IndexOf("№") + 1;

            int indexOfOrderPrice = itemText.IndexOf("Order sum: $") + 12;

            string name = itemText.Substring(indexOfName, itemText.IndexOf("|") - 1 - indexOfName);

            string phone = itemText.Substring(indexOfPhone, itemText.IndexOf("|", indexOfPhone) - 1 - indexOfPhone);

            string email = itemText.Substring(indexOfEmail, itemText.IndexOf('\r', indexOfEmail) - indexOfEmail);

            int orderNumber = Int32.Parse((itemText.Substring(indexOfOrderNumber, itemText.IndexOf("|", indexOfOrderNumber) - 1 - indexOfOrderNumber)));

            string orderPrice = ((itemText.Substring(indexOfOrderPrice, itemText.IndexOf('\r', indexOfOrderPrice) - indexOfOrderPrice)));

            orderPrice = orderPrice.Replace('.', ',');

            double orderPriceDouble = Convert.ToDouble(orderPrice);

            return new OrderItemComponent(name, phone, email, orderNumber, orderPriceDouble);
        }

        // Gets the list of Order Items from the Page
        public List<OrderItemComponent> GetOrders()
        {
            List<OrderItemComponent> orderItems = new List<OrderItemComponent>();

            foreach (var item in OrdersWebElements)
            {
                try
                {
                    orderItems.Add(ToOrderItem(item));
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Wrong element was edded to list");
                }
            }

            return orderItems;
        }
    }
}
