﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
	public class WaitingForAssignOrder:OrderComponent
	{
		public WaitingForAssignOrder(IWebElement webElement) : base(webElement)
		{

		}

		/// <summary>
		/// Clicks on Expand Button on Accepted Waiters table.
		/// </summary>
		/// <returns>Order Summary Assign Waiter Component.</returns>
		public OrderSummaryAssignWaiterComponent ExpandsOrderOnAssignedTable()
		{
			ExpandCollapseButton.Click();
			return new OrderSummaryAssignWaiterComponent(this.webElement);
		}
	}
}
