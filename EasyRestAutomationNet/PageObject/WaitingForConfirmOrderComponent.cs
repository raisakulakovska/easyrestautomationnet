﻿using OpenQA.Selenium;

namespace EasyRestAutomationNet.PageObject
{
	public class WaitingForConfirmOrderComponent: OrderComponent
	{
		public WaitingForConfirmOrderComponent(IWebElement webElement):base(webElement)
		{

		}
		
		/// <summary>
		/// Click on Expand button on Waiting for Confirm table.
		/// </summary>
		/// <returns>Order Waiting for Confirm component</returns>
		public OrderSummaryWaitingForConfirmComponent ExpandOrder()
		{
			ExpandCollapseButton.Click();
			return new OrderSummaryWaitingForConfirmComponent(this.webElement);
		}
	}
}
