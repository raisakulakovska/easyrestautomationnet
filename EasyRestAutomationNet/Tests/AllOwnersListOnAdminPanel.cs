﻿using EasyRestAutomationNet.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;

namespace EasyRestAutomationNet.Tests
{
    [TestFixture]
    public class AllOwnersListOnAdminPanel
    {
        IWebDriver driver;

        const int TIMESPAN = 1000;

        const string HOME_PAGE_URL = "http://localhost:3000";

        List<string> expectedAllUsersEmailList = new List<string> { "christianyoung@test.com",
                                                                    "earlmorrison@test.com",
                                                                    "jasonbrown@test.com",
                                                                    "jessicahendricks@test.com",
                                                                    "taylorwilliams@test.com" };

        [SetUp]
        public void SetUp()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(TIMESPAN);

            driver = new ChromeDriver();
            driver.Navigate().GoToUrl(HOME_PAGE_URL);
            driver.Manage().Window.Maximize();
        }

        private void SignInAsAdmin(string email, string password)
        {
            HeaderMenuPageComponent header = new HeaderMenuPageComponent(driver);
            header.ClickOnSignInButton();
            SignInPageObject signIn = new SignInPageObject(driver);
            signIn.Login(email, password);
        }

        private void GoToAdminPanel()
        {
            AdminHeaderMenuComponent headerAdmin = new AdminHeaderMenuComponent(driver);
            headerAdmin.ClickOnIconButton();
            headerAdmin.ClickOnAdminPanelButton();
        }

        private List<string> ClickOnOwnerOnLeftBarMenuAndGetAllOwnerList()
        {
            List<string> ownersEmailList = new List<string>();
            AdminUsersPanelPageObject page = new AdminUsersPanelPageObject(driver);
            List<UserInfoComponent> actualAllUsersList = page.LeftSidebarMenuAdministratorComponent
                                                             .ClickOnOwnersButton()
                                                             .ClickOnAllButton()
                                                             .GetUsers();
           
            foreach (var item in actualAllUsersList)
            {
                ownersEmailList.Add(item.GetEmail());
            }
            return ownersEmailList;
        }

        [Test]
        [TestCase("steveadmin@test.com", "1")]
        public void AllOwnersListAdminPanel(string email, string password)
        {
            SignInAsAdmin(email, password);
            GoToAdminPanel();
            List<string> actualAllUsersEmailList = ClickOnOwnerOnLeftBarMenuAndGetAllOwnerList();

            Assert.AreEqual(expectedAllUsersEmailList, actualAllUsersEmailList);
        }
       
        private void LogOut()
        {
            AdminHeaderMenuComponent headerDriver = new AdminHeaderMenuComponent(driver);
            headerDriver.ClickOnIconButton();
            ClientHeaderMenuComponent clientMenu = new ClientHeaderMenuComponent(driver);
            clientMenu.ClickOnLogOutButton();
        }

        [TearDown]
        public void TearDown()
        {
            LogOut();
            driver.Quit();
        }

    }
}
