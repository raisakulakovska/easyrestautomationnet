﻿using EasyRestAutomationNet.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace EasyRestAutomationNet.Tests
{
    [TestFixture]
    public class HomePageTest
    {
        private IWebDriver browser;

        private HomePageObject homePageObject;

        private const string HOME_PAGE_URL = "http://localhost:3000/";

        [SetUp]
        public void SetUp()
        {
            browser = new ChromeDriver();

            browser.Navigate().GoToUrl(HOME_PAGE_URL);

            homePageObject = new HomePageObject(browser);
        }

        [Test]
        public void UnauthorizedUserShouldBeAbleToViewHomePageTest()
        {
            homePageObject.HeaderMenuPageComponent
                          .ClickOnHomeButton();

            bool isSignInButtonDisplayed = homePageObject.HeaderMenuPageComponent
                                                         .IsSignInButtonDisplayed();

            bool isSignUpButtonDisplayed = homePageObject.HeaderMenuPageComponent
                                                         .IsSignUpButtonDisplayed();

            bool isHomeButtonDisplayed = homePageObject.HeaderMenuPageComponent
                                                       .IsHomeButtonDisplayed();

            bool isRestaurantListButtonDisplayed = homePageObject.HeaderMenuPageComponent
                                                                 .IsRestaurantListButtonDisplayed();

            bool isUserAbleToViewHomePage = isSignInButtonDisplayed
                                            && isSignUpButtonDisplayed
                                            && isHomeButtonDisplayed
                                            && isRestaurantListButtonDisplayed
                                            && browser.Url.Equals(HOME_PAGE_URL);

            Assert.True(isUserAbleToViewHomePage, "User is not able to see home page");
        }

        [TearDown]
        public void TearDown()
        {
            browser.Quit();
        }
    }
}