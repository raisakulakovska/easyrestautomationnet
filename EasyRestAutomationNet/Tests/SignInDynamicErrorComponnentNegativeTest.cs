﻿using EasyRestAutomationNet.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace EasyRestAutomationNet.Tests
{
    [TestFixture]
    class SignInDynamicErrorComponnentNegativeTest
    {
        private IWebDriver browser;
        private HomePageObject homePageObject;
        private SignInPageObject signInPageObject;

        [SetUp]
        public void SetUp()
        {
            browser = new ChromeDriver();
            const string EASYREST_HOME_PAGE_URL = "http://localhost:3000/";
            browser.Navigate().GoToUrl(EASYREST_HOME_PAGE_URL);
            browser.Manage().Window.Maximize();
            homePageObject = new HomePageObject(browser);
            signInPageObject = new SignInPageObject(browser);
        }

        [Test]
        [TestCase("test@gmail.com", "5", "Email or password is invalid")]
        public void VerifyThatUnregisteredUserCanNotToLogIn(string unregisteredEmail,
                                                            string unregisteredPassword,
                                                            string dynamicErrorMessage)
        {
            // Act
            homePageObject.HeaderMenuPageComponent.ClickOnSignInButton()
                                                  .Login(unregisteredEmail, unregisteredPassword);
            bool unregisteredUserlsIsNotLoggedIn = signInPageObject.IfDynamicErrorExist(dynamicErrorMessage);

            // Assert
            const string ASSERT_MESSAGE = "Dinamic error message is not appeared!";
            Assert.True(unregisteredUserlsIsNotLoggedIn, ASSERT_MESSAGE);

        }

        [TearDown]
        public void TearDown()
        {
            browser.Quit();
        }
    }
}