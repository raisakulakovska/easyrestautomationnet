﻿using EasyRestAutomationNet.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace EasyRestAutomationNet.Tests
{
    [TestFixture]
    public class SignInNegativeTest
    {
        private IWebDriver browser;
        private HomePageObject homePageObject;
        private SignInPageObject signInPageObject;

        [SetUp]
        public void SetUp()
        {
            browser = new ChromeDriver();
            const string EASYREST_HOME_PAGE_URL = "http://localhost:3000/";
            browser.Navigate().GoToUrl(EASYREST_HOME_PAGE_URL);
            browser.Manage().Window.Maximize();
            homePageObject = new HomePageObject(browser);
            signInPageObject = new SignInPageObject(browser);
        }

        [Test]
        [TestCase("testgmail.com", "1111", "Email is not valid")]
        public void VerifyThatWrongEmailForbidsToSignIn(string wrongEmail,
                                                        string wrongPassword,
                                                        string errorMessage)
        {
            // Act
            homePageObject.HeaderMenuPageComponent.ClickOnSignInButton()
                                                  .Login(wrongEmail, wrongPassword);
            bool userWithWrongCredentialsIsNotLoggedIn = signInPageObject.IfErrorExist(errorMessage);

            // Assert
            const string ASSERT_MESSAGE = "Error message is not appeared!";
            Assert.True(userWithWrongCredentialsIsNotLoggedIn, ASSERT_MESSAGE);
        }

        [TearDown]
        public void TearDown()
        {
            browser.Quit();
        }
    }
}