﻿using EasyRestAutomationNet.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace EasyRestAutomationNet.Tests
{
    public class SignInTest
    {
        private IWebDriver browser;
        private HomePageObject homePageObject;

        [SetUp]
        public void SetUp()
        {
            browser = new ChromeDriver();
            const string EASYREST_HOME_PAGE_URL = "http://localhost:3000/";
            browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(130);
            browser.Navigate().GoToUrl(EASYREST_HOME_PAGE_URL);
            browser.Manage().Window.Maximize();
            homePageObject = new HomePageObject(browser);
        }

        [Test]
        [TestCase("angelabrewer@test.com", "1111", "My Profile")]
        [TestCase("jasonbrown@test.com", "1111", "My restaurants")]
         
        public void VerifyThatUnauthorizedUserShouldBeAbleToSignIn(string email,
                                                                   string password,
                                                                   string buttonNameOfLoggedInUser)
        {
            // Act
            bool isUserLoggedIn = homePageObject.HeaderMenuPageComponent
                                                .ClickOnSignInButton()
                                                .Login(email, password)
                                                .ClientHeaderMenuComponent
                                                .ClickOnIconButton()
                                                .IsUserLoggedIn(buttonNameOfLoggedInUser);

            // Assert
            Assert.True(isUserLoggedIn, $"User with email {email} is not logged in.");
        }

        [TearDown]
        public void TearDoun()
        {
            MyProfilePageObject myProfilePage = new MyProfilePageObject(browser);
            myProfilePage.ClientHeaderMenuComponent.ClickOnLogOutButton();
            browser.Quit();
        }
    }
}