﻿using EasyRestAutomationNet.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Diagnostics;
using System.Linq;

namespace EasyRestAutomationNet.Tests
{
    public class WaiterPanelCloseOrderTest
    {
        private IWebDriver browser;

        private HeaderMenuPageComponent headerMenuPageComponent;

        private SignInPageObject signInPageObject;

        private InProgressTable inProgressTable;

        private WaiterHeaderMenuComponent waiterHeaderMenuComponent;

        private ProcessStartInfo resetDb;

        private const string HOME_PAGE_URL = "http://localhost:3000/";

        private const string EMAIL = "karenperez@test.com";

        private const string PASSWORD = "1";

        private const int MILLISECONDS_TO_WAIT = 500;

        private const string CMD_TEXT =
            "/C initialize_easyrest_db --reset --fill \"F:\\ITTTTTT\\SoftServe\\easyrest\\development.ini\"";

        [SetUp]    
        public void SetUp()
        {
            resetDb = new ProcessStartInfo("cmd.exe", CMD_TEXT);

            browser = new ChromeDriver();

            browser.Manage().Window.Maximize();

            signInPageObject = new SignInPageObject(browser);

            headerMenuPageComponent = new HeaderMenuPageComponent(browser);

            inProgressTable = new InProgressTable(browser);

            waiterHeaderMenuComponent = new WaiterHeaderMenuComponent(browser);
           
            browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(MILLISECONDS_TO_WAIT);

            browser.Navigate().GoToUrl(HOME_PAGE_URL);

            headerMenuPageComponent.ClickOnSignInButton();

            signInPageObject.Login(EMAIL, PASSWORD);
        }

        [Test]
        [TestCase(89)]
        public void VerifyClosingWaiterCurrentOrderTest(int orderNumber)
        {
            inProgressTable.ClickOnInProgressTable();

            inProgressTable.ClickOnExpandOrderButton(orderNumber);

            inProgressTable.ClickOnCloseOrderButton(orderNumber);

            inProgressTable.ScrolltoTopOfThePage();
            
            inProgressTable.ClickOnHistoryTableButton();
         
            var orderIsClosed = inProgressTable.GetOrders()
                                               .Any(t => t.OrderNum == orderNumber);

            Assert.IsTrue(orderIsClosed, "The order didn`t appear on history table");
        }

        [TearDown]
        public void TearDown()
        {
            waiterHeaderMenuComponent.ClickOnIconButton();

            waiterHeaderMenuComponent.ClickOnLogOutButton();

            browser.Quit();
        
            Process.Start(resetDb);
        }
    }
}