﻿using EasyRestAutomationNet.PageObject;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Diagnostics;
using System.Linq;

namespace EasyRestAutomationNet.Tests
{
    public class WaiterPanelViewCurrentOrdersTest
    {
        private IWebDriver browser;

        private HeaderMenuPageComponent headerMenuPageComponent;

        private SignInPageObject signInPageObject;

        private InProgressTable inProgressTable;

        private WaiterHeaderMenuComponent waiterHeaderMenuComponent;

        private const string HOME_PAGE_URL = "http://localhost:3000/";

        private const string EMAIL = "karenperez@test.com";

        private const string PASSWORD = "1";

        private const int MILLISECONDS_TO_WAIT = 500;     

        [SetUp]
        public void SetUp()
        {          
            browser = new ChromeDriver();

            browser.Manage().Window.Maximize();

            signInPageObject = new SignInPageObject(browser);

            headerMenuPageComponent = new HeaderMenuPageComponent(browser);

            inProgressTable = new InProgressTable(browser);

            waiterHeaderMenuComponent = new WaiterHeaderMenuComponent(browser);

            browser.Manage().Timeouts().ImplicitWait = 
                TimeSpan.FromMilliseconds(MILLISECONDS_TO_WAIT);

            browser.Navigate().GoToUrl(HOME_PAGE_URL);

            headerMenuPageComponent.ClickOnSignInButton();

            signInPageObject.Login(EMAIL, PASSWORD);
        }

        [Test]
        public void VerifyViewingCurrentOrdersTest()
        {
            inProgressTable.ClickOnInProgressTable();

            var ordersDisplayed = inProgressTable.GetOrders()
                                                 .Any();

            Assert.IsTrue(ordersDisplayed, "Failed to view current orders");
        }

        [TearDown]
        public void LogOut()
        {
            waiterHeaderMenuComponent.ClickOnIconButton();

            waiterHeaderMenuComponent.ClickOnLogOutButton();

            browser.Quit();
        }
    }
}